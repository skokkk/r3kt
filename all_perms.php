<?php
// default variables
$GLOBALS["debug"] = false;

$clientquery_port = "25639";
$fp = fsockopen("localhost", $clientquery_port);
echo fgets($fp);
echo fgets($fp);
echo fgets($fp);

include 'config.php';

// get client id
function getID($fp) {
	echo "****************************************************************\n";
	fputs($fp,"whoami\n");
	$str001 = fgets($fp);
	echo fgets($fp);
	scriptlog($str001."\n");
	$clid = explode("=",explode(" ",$str001)[0])[1];
	scriptlog($clid."\n");
	return $clid;
}

// get client database ID
function getCLDBID($fp) {
	$clid = getID($fp);
	// display client id	
	echo "*** Using Client ID: ".$clid."\n";
	fputs($fp,"clientvariable clid=".$clid." client_database_id\n");
	$str001 = fgets($fp);
	echo fgets($fp);
	scriptlog($str001."\n");
	$cldbid = explode("=",explode(" ",$str001)[1])[1];
	scriptlog($cldbid."\n");
	return $cldbid;
}

// retrieve client database ID
$cldbid = str_replace("\n", '', getCLDBID($fp));

// option 1 - all perms
foreach ( $perm as $add ) {
	// output information
	echo "*** Adding Perm: ".$add[0]."\n";
	echo "*** Using Client Database ID: ".$cldbid."\n";
	echo "*** Running: clientaddperm cldbid=".$cldbid." permsid=".$add[0]." permvalue=".$add[1]." permskip=".$add[2]."\n";
	// run actual command
    	fputs($fp,"clientaddperm cldbid=".$cldbid." permsid=".$add[0]." permvalue=".$add[1]." permskip=".$add[2]."\n");
	// display information
    	echo "*** Added perm ".$add[0]." with value ".$add[1];
	echo "\n****************************************************************\n";
	sleep($delay);
}

/*
// option 2 - basic perms
foreach ( $perm2 as $add ) {
	// retrieve client database ID
	// output information
	echo "*** Adding Perm: ".$add[0]."\n";
	echo "*** Using Client Database ID: ".$cldbid."\n";
	echo "*** Running: clientaddperm cldbid=".$cldbid." permsid=".$add[0]." permvalue=".$add[1]." permskip=".$add[2]."\n";
	// run actual command
    	fputs($fp,"clientaddperm cldbid=".$cldbid." permsid=".$add[0]." permvalue=".$add[1]." permskip=".$add[2]."\n");
	// display information
    	echo "*** Added perm ".$add[0]." with value ".$add[1];
	echo "\n****************************************************************";
}
*/



// debug log
function scriptlog($msg){
    if($GLOBALS["debug"]) {
        echo $msg;
    }
}

fclose($fp);
?>

